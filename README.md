# JetBrains License Server - GO

A license server for JetBrains products which will allow for registration of IDEs

***
Thank CrackAttackz

NOTE: This is provided for educational purposes only. CrackAttackz is not responsible for what you do with this software. Please support the software developers that create these products.
***

## Support

IntelliJ IDEA>=7.0

ReSharper>=3.1

ReSharper>=Cpp 1.0

dotTrace>=5.5

dotMemory>=4.0

dotCover>=1.0

RubyMine>=1.0

PyCharm>=1.0

WebStorm>=1.0

PhpStorm>=1.0

AppCode>=1.0

CLion>=1.0
